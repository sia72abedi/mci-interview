package com.mci.sia.service;

import com.mci.sia.exception.DataNotFoundException;
import com.mci.sia.model.domain.Product;
import com.mci.sia.model.dto.Response;
import com.mci.sia.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    public List<Product> getProducts() {
        List<Product> products = productRepository.findAll();
        if (products.isEmpty()){
            throw new DataNotFoundException("No Product Found.");
        }
        return products;
    }

    public Product getProductById(int id){
        Product product = productRepository.getAllById(id);
        if (product == null){
            throw new DataNotFoundException(MessageFormat.format("There is no product; id = {0}",id));
        }
        return product;
    }
}
