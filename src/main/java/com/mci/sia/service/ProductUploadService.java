package com.mci.sia.service;

import com.mci.sia.model.domain.Product;
import com.mci.sia.model.dto.Response;
import com.mci.sia.repository.ProductRepository;
import com.mci.sia.util.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Service
public class ProductUploadService {
    @Autowired
    private ProductRepository productRepository;
    static final int numberOfThreads = 1;

    public ResponseEntity storeCsvFile(MultipartFile csvFile) throws IOException, InterruptedException {
        ByteBuffer fileData = ByteBuffer.wrap(csvFile.getBytes());//also using OpenCSV is another solution
        List<String> lines = FileUtils.readAllLines(fileData);
        lines.remove(0);//remove header
        List<Callable<Void>> tasks = new ArrayList<>();
        for (final String line : lines) {
            final Callable<Void> task = new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    return processAndStoreCsvLine(line);
                }
            };
            tasks.add(task);
        }
        final ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
        try {
            List<Future<Void>> futures = executor.invokeAll(tasks);
            for (Future<Void> future : futures) {
                future.get();
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } finally {
            executor.shutdownNow();
        }
        return new ResponseEntity(new Response("File Uploaded Successfully."), HttpStatus.CREATED);
    }

    private Void processAndStoreCsvLine(String line) {
        Product product = new Product(line);
        productRepository.save(product);
        return null;
    }
}
