package com.mci.sia.model.domain;



import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
public class Product {

    @Id
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    @Column
    String productName;
    @Column
    BigInteger price;
    @Column
    String brand;


    public Product(String line) {
        String[] cells = line.split(",");

        this.id = Integer.parseInt(cells[0]);
        this.productName = cells[1];
        this.price = new BigInteger(cells[2]);
        this.brand = cells[3];
    }

}
