package com.mci.sia.model.dto;

import com.mci.sia.model.domain.Product;

import java.util.List;

public class CsvFile {
    private String fileName;
    private List<Product> fileContent;

    public CsvFile(String fileName, List<Product> fileContent) {
        this.fileName = fileName;
        this.fileContent = fileContent;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public List<Product> getFileContent() {
        return fileContent;
    }

    public void setFileContent(List<Product> fileContent) {
        this.fileContent = fileContent;
    }
}
