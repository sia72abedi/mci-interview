package com.mci.sia.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseBuilder;
import springfox.documentation.service.Response;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class SpringFoxConfig {                                    
    @Bean
    public Docket api() {
        List<Response> responseListGet = new ArrayList<>();
        List<Response> responseListDelete = new ArrayList<>();
        List<Response> responseListPost = new ArrayList<>();
        List<Response> responseListPut = new ArrayList<>();
        ResponseBuilder responseBuilder = new ResponseBuilder();
        responseListGet.add(responseBuilder.code("200").description("OK").build());
        responseListGet.add(responseBuilder.code("404").description("Data Not Found.").build());
        responseListDelete.add(responseBuilder.code("200").description("OK").build());
        responseListDelete.add(responseBuilder.code("404").description("Data Not Found.").build());
        responseListDelete.add(responseBuilder.code("405").description("Method Not Allowed(Data In Use)").build());
        responseListPost.add(responseBuilder.code("201").description("Created").build());
        responseListPost.add(responseBuilder.code("409").description("Conflict").build());
        responseListPut.add(responseBuilder.code("200").description("OK").build());
        responseListPut.add(responseBuilder.code("404").description("Data Not Found.").build());
        responseListGet.add(responseBuilder.code("401").description("Unauthorized").build());
        responseListGet.add(responseBuilder.code("403").description("Forbidden").build());
        responseListGet.add(responseBuilder.code("500").description("Internal Server Error").build());
        responseListDelete.add(responseBuilder.code("401").description("Unauthorized").build());
        responseListDelete.add(responseBuilder.code("403").description("Forbidden").build());
        responseListDelete.add(responseBuilder.code("500").description("Internal Server Error").build());
        responseListPost.add(responseBuilder.code("401").description("Unauthorized").build());
        responseListPost.add(responseBuilder.code("403").description("Forbidden").build());
        responseListPost.add(responseBuilder.code("500").description("Internal Server Error").build());
        responseListPut.add(responseBuilder.code("401").description("Unauthorized").build());
        responseListPut.add(responseBuilder.code("403").description("Forbidden").build());
        responseListPut.add(responseBuilder.code("500").description("Internal Server Error").build());
        return new Docket(DocumentationType.SWAGGER_2)
                .globalResponses(HttpMethod.GET, responseListGet)
                .globalResponses(HttpMethod.POST, responseListPost)
                .globalResponses(HttpMethod.PUT, responseListPut)
                .globalResponses(HttpMethod.PATCH, responseListPut)
                .globalResponses(HttpMethod.DELETE, responseListDelete)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }
}