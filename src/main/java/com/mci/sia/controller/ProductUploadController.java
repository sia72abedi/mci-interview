package com.mci.sia.controller;

import com.mci.sia.service.ProductUploadService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping(value = "/v1/product/upload")
@Api(tags="product-upload")
public class ProductUploadController {
    @Autowired
    private ProductUploadService productUploadService;


    @PostMapping(produces = "application/json")
    public ResponseEntity uploadCsvFile(@RequestParam(value="file") MultipartFile csvFile) throws IOException, InterruptedException {
        return productUploadService.storeCsvFile(csvFile);
    }
}
