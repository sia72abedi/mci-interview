package com.mci.sia.controller;

import com.mci.sia.model.domain.Product;
import com.mci.sia.service.ProductService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/v1/product")
@Api(tags="product")
public class ProductController {
    @Autowired
    private ProductService productService;


    @GetMapping(produces = "application/json")
    public List<Product> getAllProduct(){
        return productService.getProducts();
    }

    @GetMapping(value = "/{id}",produces = "application/json")
    public Product getProductById(@RequestParam int id){
        return productService.getProductById(id);
    }
}
