package com.mci.sia.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
	private FileUtils(){}
	
	public static List<String> readAllLines(ByteBuffer file) throws IOException{
		List<String>lines = new ArrayList<>();	
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(file.array())))) {
		    String line;
		    while ((line = reader.readLine()) != null) {
		    	lines.add(line);
		    }
		}
		return lines;
	}
}
