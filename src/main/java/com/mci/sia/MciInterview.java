package com.mci.sia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@SpringBootApplication
@EnableSwagger2WebMvc
@EnableJpaRepositories
public class MciInterview {

	public static void main(String[] args) {
		SpringApplication.run(MciInterview.class, args);
	}

}
