package com.mci.sia.exception;


import javax.annotation.Priority;

import com.mci.sia.model.dto.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Priority(1)
@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler
{
    @ExceptionHandler(DataNotFoundException.class)
    public final ResponseEntity<Object> handleDataNotFoundExceptions(Exception ex, WebRequest request) {
        Response response = new Response(ex.getMessage());
        logger.error(ex.getMessage());
        return new ResponseEntity(response, HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleRemainExceptions(Exception ex, WebRequest request) {
        Response response = new Response("An Error Occurred During Processing The Request.");
        logger.error("An Error Occurred During Processing The Request.",ex);
        return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}